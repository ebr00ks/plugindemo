module github.com/traefik/plugindemo

go 1.16

require (
	github.com/pborman/uuid v1.2.1
	github.com/wasmerio/wasmer-go v1.0.4
)
